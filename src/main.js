// main.js

const trafficLights = require("./traffic-light");

if (trafficLights.getTrafficHexColor("GREEN") === "#00ff00") {
    console.log("Green");
}
if (trafficLights.getTrafficHexColor("YELLOW") === "#ffff00") {
    console.log("Yellow");
}
if (trafficLights.getTrafficHexColor("RED") === "#ff0000") {
    console.log("Red");
}
if (trafficLights.getTrafficHexColor("what") === undefined) {
    console.log("Not a traffic light color");
}