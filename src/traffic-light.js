// src/traffic-light.js
const getTrafficHexColor = (word) => {
    if (word == "GREEN") {
        return "#00ff00";
    } else if (word == "YELLOW") {
        return "#ffff00";
    } else if (word == "RED") {
        return "#ff0000";
    } else {
        return undefined;
    };
};

module.exports = { getTrafficHexColor };