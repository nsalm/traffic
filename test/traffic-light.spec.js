const expect = require("chai").expect;
const { getTrafficHexColor } = require("../src/traffic-light");

describe("Traffic lights", () => {
    it("Test traffic light possibilities", () => {
        expect(true).to.equal(true);
        expect(getTrafficHexColor("RED")).to.equal("#ff0000");
        expect(getTrafficHexColor("GREEN")).to.equal("#00ff00");
        expect(getTrafficHexColor("YELLOW")).to.equal("#ffff00");
        expect(getTrafficHexColor("blue")).to.equal(undefined);
    });
});